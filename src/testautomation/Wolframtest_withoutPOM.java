package testautomation;
import java.util.List;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
public class Wolframtest_withoutPOM {
	WebDriver driver;
        @BeforeTest
    	public void setup() {
//declaration and instantiation of objects/variables  	
//TestCase 1: launch chrome and direct it to the Base URL
	
		System.setProperty("webdriver.chrome.driver","C:\\Users\\sriram\\Downloads\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);	
        driver.manage().deleteAllCookies();
        driver.get("https://www.wolframalpha.com/");
        }

        @Test
        public void program() {
 
//TestCase 2:adding values  
        driver.findElement(By.xpath("//*[@id='query']")).sendKeys("2+2");
        pause(2);
        
//TestCase 3:evaluate
        driver.findElement(By.xpath("//*[@id='input']/fieldset/div/input[2]")).click();
        
//not a functional requirement
//checking for input element
       // driver.findElement(By.xpath("//*[contains(text(),'input')]"));
        
//TestCase 4:click on open code link
        WebElement opencode = driver.findElement(By.xpath("//*[@id='Input']/section/div[2]/div/p"));
        pause(3);
        opencode.click();
        
//TestCase 5:click on play button

       List<WebElement> playbutton = driver.findElements(By.xpath("//*[contains(@class,'cell')]/div/div[1]/div/table/tbody/tr[1]/td[2]/div/div/div/div/div/div/div/div/div/div/div/div/div"));
   
       for (WebElement e : playbutton)
        {
        	e.click();
        	pause(5);
        }  
        }
        public void pause(Integer seconds)
        {
        	try
        	{
        		TimeUnit.SECONDS.sleep(seconds);
        	} catch (InterruptedException e) 
        	{
        		e.printStackTrace();
        	}
        }   
//not a functional requirement.
//just to make sure we get the text on RHS of the notebook.
         //driver.findElement(By.xpath("//*[contains(text(),'Things to Try: Calculus')]")).click();
           

      @AfterTest
      public void close() 
      {
//TestCase 6:close Google Chrome
    	  driver.close();       
          driver.quit();
       } 
      
public static void main(String[] args) {
	
	Wolframtest_withoutPOM obj = new Wolframtest_withoutPOM();
	obj.setup();
    }
}