/**
 * 
 */
package Utility;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;

/**
 * @author sriram
 *
 */
public class BrowserFactory {

	static WebDriver driver;
	public static WebDriver startbrowser(String browserName,String URL)
	{
		System.setProperty("webdriver.chrome.driver","C:\\Users\\sriram\\Downloads\\chromedriver_win32\\chromedriver.exe");
		if (browserName.equalsIgnoreCase("chrome"))
		{
			driver = new ChromeDriver();
		}
		else if (browserName.equalsIgnoreCase("firefox"))
		{
			driver = new FirefoxDriver();
		}
		else if (browserName.equalsIgnoreCase("IE"))
		{
			driver = new InternetExplorerDriver();
		}
		driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);	
        driver.manage().deleteAllCookies();
        driver.get(URL);
        return driver;
	}
	
	
	
	
	
	
}
