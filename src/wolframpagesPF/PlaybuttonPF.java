/**
 * 
 */
package wolframpagesPF;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wolfram.testcasses.WolframtestPF;
//import org.openqa.selenium.support.CacheLookup;
//import org.openqa.selenium.support.FindBy;
//import org.openqa.selenium.support.How;

/**
 * @author sriram
 *
 */
public class PlaybuttonPF {
	WebDriver driver;

	public PlaybuttonPF(WebDriver driver)
	{
		this.driver = driver;
	}
	public void play() {
		
		List<WebElement> playbutton = driver.findElements(By.xpath("//*[contains(@class,'cell')]/div/div[1]/div/table/tbody/tr[1]/td[2]/div/div/div/div/div/div/div/div/div/div/div/div/div"));
		   
	       for (WebElement e : playbutton)
	        {
	        	e.click();
	        	WolframtestPF.pause(5);
	        } 
	}
}
