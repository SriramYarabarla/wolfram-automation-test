/**
 * 
 */
package wolframpagesPF;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author sriram
 *
 */
public class entervaluesPF {

	//@FindBy(id="query") WebElement query;
	
	@CacheLookup
	@FindBy(how=How.ID,using="query") WebElement querysearch;
	
	@CacheLookup
	@FindBy(how=How.XPATH,using="//*[@id='input']/fieldset/div/input[2]") WebElement querysearchClick;
	
	 public void entervalues(String values) {
			
			querysearch.sendKeys(values);
		}
	    public void click()
	    {
	    	querysearchClick.click();
	    }	
}
