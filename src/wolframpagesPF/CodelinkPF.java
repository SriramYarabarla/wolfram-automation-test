/**
 * 
 */
package wolframpagesPF;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * @author sriram
 *
 */
public class CodelinkPF {

	@CacheLookup
	@FindBy(how=How.XPATH,using="//*[@id='Input']/section/div[2]/div/p") WebElement opencode;

	public void opencode() {	
		opencode.click();
	}	
	
}
