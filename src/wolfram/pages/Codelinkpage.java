/**
 * 
 */
package wolfram.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
/**
 * @author sriram
 *
 */
public class Codelinkpage {	
    WebDriver driver;	
    //POM model
	By opencode = By.xpath("//*[@id='Input']/section/div[2]/div/p");
	
    public Codelinkpage(WebDriver driver) {
    	this.driver = driver;
	}
	public void opencode() {
		
		driver.findElement(opencode).click();
	}	
}
