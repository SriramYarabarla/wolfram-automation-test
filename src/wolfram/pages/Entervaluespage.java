/**
 * 
 */
package wolfram.pages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * @author sriram
 *
 *
 *This class will store all locators and methods of Entervalues page.
 */
public class Entervaluespage {

	WebDriver driver;
	
	By querysearch = By.xpath("//*[@id='query']");
	By querysearchClick = By.xpath("//*[@id='input']/fieldset/div/input[2]");
	
    public Entervaluespage(WebDriver driver) 
    {
    	this.driver = driver;
    }
    public void entervalues(String values) {
		
		driver.findElement(querysearch).sendKeys(values);
	}
    public void click()
    {
    	driver.findElement(querysearchClick).click();
    }
	


}


