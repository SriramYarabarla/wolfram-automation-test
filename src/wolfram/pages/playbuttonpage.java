package wolfram.pages;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import wolfram.testcasses.Wolframtest;

public class playbuttonpage {
	WebDriver driver;
	
	public playbuttonpage(WebDriver driver) {
    	this.driver = driver;
	}
	
	public void play()
	{	
		List<WebElement> playbutton = driver.findElements(By.xpath("//*[contains(@class,'cell')]/div/div[1]/div/table/tbody/tr[1]/td[2]/div/div/div/div/div/div/div/div/div/div/div/div/div"));
		   
	       for (WebElement e : playbutton)
	        {
	        	e.click();
	        	Wolframtest.pause(5);
	        } 
	}
}
	