package wolfram.testcasses;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;
import wolfram.pages.Codelinkpage;
import wolfram.pages.Entervaluespage;
import wolfram.pages.playbuttonpage;

public class Wolframtest {
	WebDriver driver;
	@Test
	public void automate()
	{
		System.setProperty("webdriver.chrome.driver","C:\\Users\\sriram\\Downloads\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);	
        driver.manage().deleteAllCookies();
        driver.get("https://www.wolframalpha.com/");
        
        Entervaluespage entervalues = new Entervaluespage(driver);
        Codelinkpage codeclick = new Codelinkpage(driver);
        playbuttonpage playbutton = new playbuttonpage(driver);
        
        pause(3);
        entervalues.entervalues("2+2");
        entervalues.click();
        pause(5);
        codeclick.opencode();
        pause(5);
        playbutton.play();
        pause(5);
        driver.close();
        driver.quit();
	}
	 public static void pause(Integer seconds)
     {
     	try
     	{
     		TimeUnit.SECONDS.sleep(seconds);
     	} catch (InterruptedException e) 
     	{
     		e.printStackTrace();
     	}
     } 
	
}
