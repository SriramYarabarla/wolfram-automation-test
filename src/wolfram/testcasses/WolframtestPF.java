/**
 * 
 */
package wolfram.testcasses;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;
import Utility.BrowserFactory;
import wolframpagesPF.CodelinkPF;
import wolframpagesPF.PlaybuttonPF;
import wolframpagesPF.entervaluesPF;

public class WolframtestPF {

	@Test
	public void automate()
	{
		// This will launch browser and specific URL
	WebDriver driver =	BrowserFactory.startbrowser("chrome", "https://www.wolframalpha.com/");
	
	// Created Page Object of entervaluesPF using Page Factory
	entervaluesPF entervalues  = PageFactory.initElements(driver, entervaluesPF.class);	
	// Call the method
	entervalues.entervalues("2+2");
	entervalues.click();
	
	// Created Page Object of CodelinkPF using Page Factory
	CodelinkPF Codelink  = PageFactory.initElements(driver, CodelinkPF.class);	
	// Explicit Wait for better view of results
	pause(2);
	// Call the method
	Codelink.opencode();
	
	// Created Page Object of CodelinkPF using Page Factory
	PlaybuttonPF playbutton  = PageFactory.initElements(driver, PlaybuttonPF.class);	
	// Explicit Wait for better view of results
	pause(3);
	// Call the method
	playbutton.play();
	// Explicit Wait for better view of results
	pause(5);
	//close driver.
	driver.close();
    driver.quit();
	}
	 public static void pause(Integer seconds)
     {
     	try
     	{
     		TimeUnit.SECONDS.sleep(seconds);
     	} catch (InterruptedException e) 
     	{
     		e.printStackTrace();
     	}
     } 
}
